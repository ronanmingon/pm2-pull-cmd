var pm2 = require('pm2');
var pmx = require('pmx');
const { spawn, exec } = require("child_process");
const simpleGit = require('simple-git');
const git = simpleGit();

/******************************
 *    ______ _______ ______
 *   |   __ \   |   |__    |
 *   |    __/       |    __|
 *   |___|  |__|_|__|______|
 *
 *      PM2 Module Sample
 *
 ******************************/

function autoPull(cb) {
  pm2.list(function(err, procs) {

    procs.forEach(async proc => {
      if (proc.pm2_env && proc.pm2_env.versioning && !proc.pm2_env.pmx_module) {
        const path = proc.pm2_env.versioning.repo_path
        const options = {
          baseDir: proc.pm2_env.pm_cwd,
          binary: 'git',
          maxConcurrentProcesses: 6,
        };
        const git = simpleGit(options)
        let pull = await git.pull()
        const package = require(`${path}/package.json`)
        if (pull.files.length) {
          console.log("pull new data")
          const ls = spawn(`cd ${path} && ${package.cicd.afterPull}`);

          ls.stdout.on("data", data => {
            console.log(`stdout: ${data}`);
          });

          ls.stderr.on("data", data => {
            console.log(`stderr: ${data}`);
          });

          ls.on('error', (error) => {
            console.log(`error: ${error.message}`);
          });

          ls.on("close", code => {
            console.log(`child process exited with code ${code}`);
            if (package.cicd.restartProcess) {
              exec(`pm2 restart ${proc.pm2_env.pm_id}`, (error, stdout, stderr) => {
                if (error) {
                  console.log(`error: ${error.message}`);
                  return;
                }
                if (stderr) {
                  console.log(`stderr: ${stderr}`);
                  return;
                }
                console.log(`stdout: ${stdout}`);
              })
            }
          })
        } else {
          console.log("nothing pull")
        }
      }
    })

  });
}

pmx.initModule({
  widget : {
    logo : 'https://app.keymetrics.io/img/logo/keymetrics-300.png',
    theme            : ['#141A1F', '#222222', '#3ff', '#3ff'],
    el : {
      probes  : true,
      actions : true
    },
    block : {
      actions : false,
      issues  : true,
      meta    : true,
      main_probes : ['test-probe']
    }

  }

}, function(err, conf) {
  pm2.connect(function() {
    console.log('pm2-pull-cmd module connected to pm2');

    var running = false;

    setInterval(function() {
      if (running == true) return false;

      running = true;
      autoPull(function() {
        running = false;
      });
    }, conf.interval || 30000);

  });

});
